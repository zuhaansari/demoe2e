package com.citi.demoe2e;

import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.List;
import com.citi.demoe2e.entities.Trade;
import com.citi.demoe2e.services.TradeService;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ServiceTests {

    @Autowired
    TradeService tradeservice;

    @Test
    public void testFinAllReturnsTrades(){

        List<Trade> trades = tradeservice.findAll();
        int actual = trades.size();

        assertTrue(actual>=0);
    }
}
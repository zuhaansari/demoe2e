package com.citi.demoe2e.entities;

import java.util.Date;

import com.citi.demoe2e.TradeException;

import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Trade extends Entity {
    
    private double volume;
    private String tradetype;
    private String status;
    private Date tradedate;
    private String transcode;

    public double getVolume() {
        return volume;
    }

    public void setVolume(double volume) {
        if (volume <0){
            throw new TradeException("Volume must be > 0");
        }
        else{
            this.volume = volume;
        }
    }

    public String getTradetype() {
        return tradetype;
    }

    public void setTradetype(String tradetype) {
        this.tradetype = tradetype;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getTradedate() {
        return tradedate;
    }

    public void setTradedate(Date tradedate) {
        this.tradedate = tradedate;
    }

    public String getTranscode() {
        return transcode;
    }

    public void setTranscode(String transcode) {
        this.transcode = transcode;
    }

    public Trade(int id, Date cdate, double volume, String tradetype, String status, Date tradedate, String transcode) {
        super(id, cdate);
        this.volume = volume;
        this.tradetype = tradetype;
        this.status = status;
        this.tradedate = tradedate;
        this.transcode = transcode;
    }

    public Trade(){

    }

    @Override
    public String toString() {
        return "Trade [status=" + status + ", tradedate=" + tradedate + ", tradetype=" + tradetype + ", transcode="
                + transcode + ", volume=" + volume + "]";
    }
    
}
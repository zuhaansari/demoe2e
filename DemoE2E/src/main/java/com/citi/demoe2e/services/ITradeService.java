package com.citi.demoe2e.services;

import java.util.List;
import com.citi.demoe2e.entities.Trade;

public interface ITradeService {
    List<Trade> findAll();
    Trade findbyId(int id);
    int add (Trade trade);
    

    
}
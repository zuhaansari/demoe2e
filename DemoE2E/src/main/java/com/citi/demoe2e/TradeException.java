package com.citi.demoe2e;

public class TradeException extends RuntimeException{

    /**
     *
     */
    private static final long serialVersionUID = 4580837492422818727L;
    //Todo configure log4j - append to a log file

    public TradeException(String message) {
        super(message);
    }
    
}
package com.citi.demoe2e.dao;

import java.util.List;

import com.citi.demoe2e.entities.Trade;

public interface TradeDao {
    Long rowCount();
    List<Trade> findAll();
    Trade findById(int id);
    int place(Trade trade);
    Trade updateState(Trade trade);
    int clear();


}
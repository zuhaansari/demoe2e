package com.citi.demoe2e;

import com.citi.demoe2e.entities.Trade;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class EntitiesTest {

    @Test
    public void TestVolumneGenerateATradeException(){

        Trade trade = new Trade();
        trade.setVolume(-1);

    }


    Exception exception = Assertions.assertThrows(TradeException.class, ()->{
            Trade trade = new Trade();
            trade.setVolume(-9);
        }
    );
}
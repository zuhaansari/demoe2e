package com.citi.demoe2e.dao;

import java.util.List;
import com.citi.demoe2e.entities.Trade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

@Repository // bean
public class TradeDaoMongo implements TradeDao {

    @Autowired
    private MongoTemplate tpl;
    

    @Override
    public Long rowCount() {
       Query query = new Query();
       Long result= tpl.count(query, Trade.class);
       return result;
    }

    @Override
    public List<Trade> findAll() {
        return tpl.findAll(Trade.class);
    }

    @Override
    public Trade findById(int id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        Trade trade = tpl.findOne(query, Trade.class);
        return trade;
    }

    @Override
    public int place(Trade trade) {
        tpl.insert(trade);
        return 0;
    }

    @Override
    public Trade updateState(Trade trade) {
        return null;
    }

    @Override
    public int clear() {
        return 0;
    }
    
}
package com.citi.demoe2e.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import com.citi.demoe2e.entities.Trade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;


@Repository //bean
public class TradeDaoMySQL implements TradeDao {

    @Autowired 
    private JdbcTemplate tpl;


    @Override
    public Long rowCount() {
        return tpl.queryForObject("select count(*) from Trades", Long.class);
    }

    @Override
    public List<Trade> findAll() {
        return this.tpl.query("select * from Trades", new TradeMapper());
    }

    private static final class TradeMapper implements RowMapper<Trade> {
        public Trade mapRow(ResultSet rs, int rowNum) throws SQLException { 
             Trade trade = new Trade();
            trade.setId(rs.getInt("id"));
			trade.setTradedate(rs.getTimestamp("tradedate"));	
			trade.setVolume(rs.getDouble("amt"));
            trade.setTradetype(rs.getString("tradetype"));  
            trade.setTranscode(rs.getString("transcode"));  
		return trade;
        }
   }


    @Override
    public Trade findById(int id) {
        return null;
    }

    @Override
    public int place(Trade trade) {
        return 0;
    }

    @Override
    public Trade updateState(Trade trade) {
        return null;
    }

    @Override
    public int clear() {
        return 0;
    }
    
}
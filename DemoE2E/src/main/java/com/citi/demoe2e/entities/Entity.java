package com.citi.demoe2e.entities;

import java.util.Date;

import org.springframework.data.annotation.Id;

public class Entity {
    @Id
    private int id;
    private Date cdate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCdate() {
        return cdate;
    }

    public void setCdate(Date cdate) {
        this.cdate = cdate;
    }

    public Entity(int id, Date cdate) {
        this.id = id;
        this.cdate = cdate;
    }

    public Entity(){

    }

    @Override
    public String toString() {
        return super.toString()+"Entity [cdate=" + cdate + ", id=" + id + "]";
    }
}
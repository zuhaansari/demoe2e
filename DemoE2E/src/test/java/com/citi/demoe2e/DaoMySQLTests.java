package com.citi.demoe2e;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import com.citi.demoe2e.dao.TradeDaoMySQL;
import com.citi.demoe2e.entities.Trade;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class DaoMySQLTests {

    @Autowired
    TradeDaoMySQL tradeDaoMySQL;

    @Test
    public void TestRowCountIsGreaterThanOrEqualZero(){
        Long actual = tradeDaoMySQL.rowCount();
        assertTrue(actual>=0);
    }

    @Test()
    public void TestFindAllReturns1orMoreTrades(){
        List<Trade> trades = tradeDaoMySQL.findAll();
        int actual = trades.size();

        assertTrue(actual>=0);
    }
}
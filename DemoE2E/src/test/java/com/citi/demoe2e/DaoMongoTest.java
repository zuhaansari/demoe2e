package com.citi.demoe2e;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Date;
import com.citi.demoe2e.dao.TradeDaoMongo;
import com.citi.demoe2e.entities.Trade;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class DaoMongoTest {
    
    @Autowired
    TradeDaoMongo tradeDaoMongo;

    @Test
     public void CheckTradeInsertAddsDocumenttoDB(){

        Date dt = new Date(2020,1,1);
        Trade trade = new Trade(3, dt, 300, "Buy", "Accept", dt, "test");
        tradeDaoMongo.place(trade);

        trade = tradeDaoMongo.findById(3);
        assertNotNull(trade, "Trade inserted");
    }
}
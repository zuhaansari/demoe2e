package com.citi.demoe2e.rest;

import java.util.List;
import com.citi.demoe2e.entities.Trade;
import com.citi.demoe2e.services.TradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping ("/trades")
public class TradeController {
    
    @Autowired
    private TradeService tradeService;

    @RequestMapping (value = "/getstatus", method = RequestMethod.GET)
    public String getstatus(){
        return "Web Server is running";
    }

    @RequestMapping(value = "getAll", method = RequestMethod.GET)
    public List<Trade> getAll(){
        return tradeService.findAll();
    }
}
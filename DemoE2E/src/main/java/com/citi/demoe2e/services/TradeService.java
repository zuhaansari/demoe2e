package com.citi.demoe2e.services;

import java.util.List;
import com.citi.demoe2e.dao.TradeDaoMySQL;
import com.citi.demoe2e.entities.Trade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TradeService implements ITradeService {

    @Autowired
    TradeDaoMySQL dao;
    
	@Override
	public List<Trade> findAll() {
		return dao.findAll();
	}

	@Override
	public Trade findbyId(int id) {
		return dao.findById(id);
	}

    @Override
    public int add(Trade trade) {
        return dao.place(trade);
    }
    
}
package com.citi.demoe2e;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Demoe2eApplication {

	public static void main(String[] args) {
		SpringApplication.run(Demoe2eApplication.class, args);
	}

}
